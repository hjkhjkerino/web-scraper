import requests
import json
from bs4 import BeautifulSoup as BSoup
import string
import os

def get_page_content(url, page):
    """requests page content from url"""
    r = requests.get(url, params={'page': page})
    if r.status_code != 200:
        print(f'Status code: {r.status_code}')
    else:
        return r.content

def get_article_title(page):
    """get article title from page"""
    parsed_title = BSoup(page, 'html.parser')
    title = parsed_title.title.string
    return title

def get_article_text(page):
    parsed_text = BSoup(page, 'html.parser')
    text = parsed_text.find("div", class_="article-item__body")
    return text.get_text()

def get_file_name(title):
    split_title = str(title).split(':')[0]
    stripped_title = str(split_title).strip()
    trans = stripped_title.maketrans(' ', '_', string.punctuation)
    return stripped_title.translate(trans) + '.txt'

def save_file(title, text):
    file = open(title, 'w', encoding='utf-8')
    file.write(text)
    file.close()

def find_articles(page, type):
    article_urls = []
    url = f'https://www.nature.com/nature/articles'
    r = get_page_content(url, page)
    soup = BSoup(r, 'html.parser')
    all_articles = soup.find_all('article')
    for article in all_articles:
        if article.find(class_="c-meta__type").string == type:
            article_href = article.find(attrs={'data-track-action': 'view article'}).get('href')
            article_urls.append(f'https://nature.com{article_href}')
    return article_urls

def make_dir(num_page):
    for i in range(1, num_page + 1):
        dir_name = f'Page_{i}'
        os.mkdir(dir_name)

num_pages = int(input())
article_type = input()
make_dir(num_pages)
for i in range(1, num_pages + 1):
    page_art_urls = find_articles(i, article_type)
    dir_name = f'Page_{i}'
    os.chdir(dir_name)
    for url in page_art_urls:
        page_content = get_page_content(url, i)
        article_title = get_article_title(page_content)
        article_text = str(get_article_text(page_content)).strip()
        file_name = get_file_name(article_title)
        save_file(file_name, article_text)
    os.chdir(os.path.dirname(os.getcwd()))