# the list "walks" is already defined
# your code here
sum_walks = 0
num_walks = 0
for walk in walks:
    sum_walks = sum_walks + int(walk.get("distance", 0))
    num_walks += 1
print(int(sum_walks / num_walks))
